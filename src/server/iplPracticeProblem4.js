// Find the highest number of times one player has been dismissed by another player

const csvToJsonConvertor = require('csvtojson')
const path = require('path')
const fs = require('fs')

const deliveriesCSVFilePath = path.resolve(__dirname,'../data/deliveries.csv')
const resultJsonFilePathPractice4 = path.resolve(__dirname,'../public/output/practice4.json')

const getMaxAPlayerOutByAnother = function(deliveries) {
    let resultObject = {}
    let dismissalType2 = ['obstructing the field','retired hurt','run out','']
    for (const delivery of deliveries) {
        let dismissal = delivery.dismissal_kind
        if(!dismissalType2.includes(dismissal)) {
            if(!resultObject[delivery.player_dismissed]) {
                resultObject[delivery.player_dismissed] = {} ;
            }
            if(!resultObject[delivery.player_dismissed][delivery.bowler]) {
                resultObject[delivery.player_dismissed][delivery.bowler] = 0 ;
            }
            resultObject[delivery.player_dismissed][delivery.bowler] += 1 ;
        }
    }
    for (const key in resultObject) {
        let dismissedValueArray = Object.values(resultObject[key])
        dismissedValueArray.sort(function(a,b){return b-a})
        let newObj = {}
        for(const subkey in resultObject[key]) {
            if(dismissedValueArray[0] == resultObject[key][subkey]) {
                newObj[subkey] = dismissedValueArray[0]
            }
        }
        resultObject[key] = newObj;
    }
    return resultObject
}

const testCall = async () => {
    const deliveriesJSData = await csvToJsonConvertor().fromFile(deliveriesCSVFilePath).then((JsonObj) => JsonObj) ;
    const result = getMaxAPlayerOutByAnother(deliveriesJSData);
    fs.writeFileSync(resultJsonFilePathPractice4,JSON.stringify(result,null,2),err => {console.log(err)})
}

testCall()
