// Find the number of times each team won the toss and also won the match 

const csvToJsonConvertor = require('csvtojson')
const path = require('path')
const fs = require('fs')

const matchesCSVFilePath = path.resolve(__dirname,'../data/matches.csv')
const resultJsonFilePathPractice1 = path.resolve(__dirname,'../public/output/practice1.json')

const getTeamsWhoWonTossAndMatch = (matches) => {
    let resultObject = {}
    for ( const match of matches ) {
        if(match.toss_winner == match.winner) {
            if(!resultObject[match.toss_winner]) {
                resultObject[match.toss_winner] = 1 
            }
            resultObject[match.toss_winner] += 1 
        }
    }
    return resultObject
}

const testCall = async () => {
    const matchesJSData = await csvToJsonConvertor().fromFile(matchesCSVFilePath).then((JsonObj) => JsonObj) ;
    const result = getTeamsWhoWonTossAndMatch(matchesJSData);
    fs.writeFileSync(resultJsonFilePathPractice1,JSON.stringify(result,null,2),err => {console.log(err)})
}

testCall()