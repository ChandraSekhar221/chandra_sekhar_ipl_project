const csvToJsonConvertor = require('csvtojson')
const path = require('path')
const fs = require('fs')

const matchesCSVFilePath = path.resolve(__dirname,'../data/matches.csv')
const deliveriesCSVFilePath = path.resolve(__dirname,'../data/deliveries.csv')
const resultJsonFilePath = path.resolve(__dirname,'../public/output/extraRunsConceededPerTeamIn2016.json')

const getExtraRunsConceededByTeam = async () => {
    const matches = await csvToJsonConvertor().fromFile(matchesCSVFilePath).then((responce) => responce) 
    const deliveries = await csvToJsonConvertor().fromFile(deliveriesCSVFilePath).then((responce) => responce)

    const year = 2016 ;
    const matchIdsArray = matches.reduce((startingValue,match) => {
        if(match.season == year) {
            startingValue.push(match.id)
        }
        return startingValue
    }, [])
    
    const resuslt = deliveries.reduce((startingValue,ball)=> {
        if(matchIdsArray.includes(ball.match_id)) {
            if(!startingValue[ball.bowling_team]) {
                startingValue[ball.bowling_team] = 0 
            }
            startingValue[ball.bowling_team] += parseFloat(ball.extra_runs)
        }
        return startingValue
    },{})
    
    fs.writeFileSync(resultJsonFilePath,JSON.stringify(resuslt,null,2), (err) => { console.log(err)})
}

getExtraRunsConceededByTeam()
