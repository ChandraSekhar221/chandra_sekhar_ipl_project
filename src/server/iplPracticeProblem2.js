// Find a player who has won the highest number of Player of the Match awards for each season

const csvToJsonConvertor = require('csvtojson')
const path = require('path')
const fs = require('fs')

const matchesCSVFilePath = path.resolve(__dirname,'../data/matches.csv')
const resultJsonFilePathPractice2 = path.resolve(__dirname,'../public/output/practice2.json')

const getPlayerwithMaxMOMAwardsSeasonwise = (matches) => {
    let resultObject = {}
    for ( const match of matches ) {
        if(!resultObject[match.season]) {
            resultObject[match.season] = {} ;
        }if(!resultObject[match.season][match.player_of_match]) {
            resultObject[match.season][match.player_of_match] = 0 ;
        }
        resultObject[match.season][match.player_of_match] += 1 ;
    }
    for(const key in resultObject) {
        let maxValue =  Object.values(resultObject[key]).sort((a,b) => b-a )[0]
        let subObj = {}
        for (const subkey in resultObject[key]) {
            if(maxValue ==  resultObject[key][subkey]) {
                subObj[subkey] = maxValue
            }
        }
        resultObject[key] = subObj
    }
    return resultObject
}

const testCall = async () => {
    const matchesJSData = await csvToJsonConvertor().fromFile(matchesCSVFilePath).then((JsonObj) => JsonObj) ;
    const result = getPlayerwithMaxMOMAwardsSeasonwise(matchesJSData);
    fs.writeFileSync(resultJsonFilePathPractice2,JSON.stringify(result,null,2),err => {console.log(err)})
}

testCall()