// Find a player who has won the highest number of Player of the Match awards for each season

const csvToJsonConvertor = require('csvtojson')
const path = require('path')
const fs = require('fs')

const matchesCSVFilePath = path.resolve(__dirname,'../data/matches.csv')
const deliveriesCSVFilePath = path.resolve(__dirname,'../data/deliveries.csv')
const resultJsonFilePathPractice3 = path.resolve(__dirname,'../public/output/practice3.json')

const findStrikeRateOfBatsmen = (matches,deliveries) => {
    let yearAndMatchId = {}
    let resultObj = {}
    for(const match of matches ) {
        if(!yearAndMatchId[match.season]) {
            yearAndMatchId[match.season] = []
        }if(!yearAndMatchId[match.season].includes(match.id)) {
            yearAndMatchId[match.season].push(match.id)
        }
    }
    for (const delivery of deliveries) {
        for(const year in yearAndMatchId) {
            if(yearAndMatchId[year].includes(delivery.match_id)){
                if(!resultObj[delivery.batsman]) {
                    resultObj[delivery.batsman] = {}
                }if(!resultObj[delivery.batsman][year]) {
                    resultObj[delivery.batsman][year] = {'balls_faced':0,'runs_scored':0,'strike_rate':0}
                }
                resultObj[delivery.batsman][year]['runs_scored'] += parseFloat(delivery.batsman_runs)
                if(delivery.wide_runs == 0 && delivery.noball_runs){
                    resultObj[delivery.batsman][year]['balls_faced'] += 1
                }if(resultObj[delivery.batsman][year]['balls_faced'] > 0 ) {
                    resultObj[delivery.batsman][year]['strike_rate'] = (
                        resultObj[delivery.batsman][year]['runs_scored'] * 100 /
                        resultObj[delivery.batsman][year]['balls_faced']
                    ).toFixed(2)
                }
            }
        }
    }
    for (const key in resultObj) {
        let reAssignObj = {}
        for(const subkey in resultObj[key]) {
            reAssignObj[subkey] = resultObj[key][subkey]['strike_rate']
        }
        resultObj[key] = reAssignObj ;
    }
    return resultObj
}

const testCall = async () => {
    const matchesJSData = await csvToJsonConvertor().fromFile(matchesCSVFilePath).then((JsonObj) => JsonObj) ;
    const deliveriesJSData = await csvToJsonConvertor().fromFile(deliveriesCSVFilePath).then((JsonObj) => JsonObj) ;
    const result = findStrikeRateOfBatsmen(matchesJSData,deliveriesJSData);
    fs.writeFileSync(resultJsonFilePathPractice3,JSON.stringify(result,null,2),err => {console.log(err)})
}

testCall()