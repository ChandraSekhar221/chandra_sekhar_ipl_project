const csvToJsonConvertor = require('csvtojson');
const path = require('path');
const fs = require('fs');

const resultJsonFilePath = path.resolve(__dirname,'../public/output/matchesPerYear.json');
const matchesCsvFilePath = path.resolve(__dirname,'../data/matches.csv');

const getMatchesPerYear = async () => {
    const matches = await csvToJsonConvertor().fromFile(matchesCsvFilePath).then((jsonObject) => jsonObject)
    const result = matches.reduce((startingValue,match) => {
        if(!startingValue[match.season]) {
            startingValue[match.season] = 0 ;
        }
        startingValue[match.season] += 1 ;
        return startingValue ;
    }, {})
    fs.writeFileSync(resultJsonFilePath,JSON.stringify(result,null,2),(err)=> {console.log(err)})
}

getMatchesPerYear();