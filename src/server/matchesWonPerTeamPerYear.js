const csvToJsonConvertor = require('csvtojson') ;
const path = require('path');
const fs = require('fs');

const mathchesCSVFilepath = path.resolve(__dirname,'../data/matches.csv');
const outputJsonFilePath = path.resolve(__dirname,'../public/output/matchesWonPerTeamPerYear.json');

const getMatchesWonByTeamPerSeason = async () => {
    const matches = await csvToJsonConvertor().fromFile(mathchesCSVFilepath).then((jsonObj) => jsonObj);
    const result = matches.reduce((startingValue,match)=> {
        if(!startingValue[match.winner]) {
            startingValue[match.winner] = {} ;
        }
        if(!startingValue[match.winner][match.season]) {
            startingValue[match.winner][match.season] = 0 ;
        }
        startingValue[match.winner][match.season] += 1 ;
        
        return startingValue ;
    }, {})

    fs.writeFileSync(outputJsonFilePath,JSON.stringify(result,null,2),(err) => {console.log(err)})
}

getMatchesWonByTeamPerSeason();
