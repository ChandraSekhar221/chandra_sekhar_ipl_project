const {getMatchesPerYear} = require('./ipl.js') ;
const {matchesWonPerYear} = require('./ipl.js') ;
const {getExtraRunsConceededByTeam} = require('./ipl.js') ;
const {getEconomyOfBowels} = require('./ipl.js') ;



const csvtojson = require('csvtojson') ;
const path = require('path') ;
const fs = require('fs') ;

const matchesCsvFilePath = path.resolve(__dirname,'../data/matches.csv');
const deliveriesCsvFilePath = path.resolve(__dirname,'../data/deliveries.csv');

const matchesPerYearFilePath = path.resolve(__dirname,'../public/output/matchesPerYear.json')
const matchesWonPerYearFilePath = path.resolve(__dirname,'../public/output/matchesWonPerTeamPerYear.json')
const extraRunsConceededJsonFilePath = path.resolve(__dirname,'../public/output/extraRunsConceededPerTeamIn2016.json')
const top10EconomicalBowlersJsonPath = path.resolve(__dirname,'../public/output/top10EconomicalBowlersIn2015.json')



const ipltest= async () => {  
    const matchesJson = await csvtojson().fromFile(matchesCsvFilePath).then((jsomObj) => { return jsomObj})
    const deliveriesJson = await csvtojson().fromFile(deliveriesCsvFilePath).then((jsomObj) => { return jsomObj})


    const matchesPerYear  = getMatchesPerYear(matchesJson)
    fs.writeFileSync(matchesPerYearFilePath,JSON.stringify(matchesPerYear,null,2),(err) => { if(err) console.log(err)} )

    const matchesWonByYearByTeam = matchesWonPerYear(matchesJson)
    fs.writeFileSync(matchesWonPerYearFilePath ,JSON.stringify(matchesWonByYearByTeam,null,2),(err) => { if(err) console.log(err)} )

    const extraRunsConceeded = getExtraRunsConceededByTeam(matchesJson,deliveriesJson,2016);
    fs.writeFileSync(extraRunsConceededJsonFilePath ,JSON.stringify(extraRunsConceeded,null,2),(err) => { if(err) console.log(err)} )

    const top10EconomicalBowlers = getEconomyOfBowels(matchesJson,deliveriesJson,2015);
    fs.writeFileSync(top10EconomicalBowlersJsonPath ,JSON.stringify(top10EconomicalBowlers,null,2),(err) => { if(err) console.log(err)} )

}
    
ipltest()

