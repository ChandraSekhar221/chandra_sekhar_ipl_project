const getMatchesPerYear = (testObj) => {
    let matchesPerYear = {}
    for(const match of testObj) {
        if (!matchesPerYear[match.season]) {
            matchesPerYear[match.season] = 1
        } else {
            matchesPerYear[match.season] += 1 ;
        }
    }
    return matchesPerYear
}

module.exports.getMatchesPerYear = getMatchesPerYear ;

const matchesWonPerYear = (testObj) => {
    let myObject = {}
    for ( const match of testObj) {
        let winner = match.winner 
        if(myObject[winner]) {
            myObject[winner]['Total matches won'] += 1 ;
            if(myObject[winner][match.season]) {
                myObject[winner][match.season] += 1 ;
            } else {
                myObject[winner][match.season] = 1
            }
        }else {
            let item = [[match.season,1],['Total matches won',1]]
            myObject[winner] = Object.fromEntries(item)
        }
    }
    return myObject ;
}

module.exports.matchesWonPerYear = matchesWonPerYear ;

const getExtraRunsConceededByTeam = (matches,deliveries,year) => {
    if(!matches || !deliveries || !year) return {}
    let myObject = {}
    for(const match of matches) {
        if (year == match.season) {
            let matchId = match.id
            for (const delivery of deliveries) {
                if(delivery.match_id == matchId) {
                    if(myObject[delivery.bowling_team]) {
                        myObject[delivery.bowling_team] = myObject[delivery.bowling_team] + parseFloat(delivery.extra_runs) ;
                    } else {
                        myObject[delivery.bowling_team] = parseFloat(delivery.extra_runs)
                    }
                } 
            }
        }

    }
    return myObject ;
}

module.exports.getExtraRunsConceededByTeam = getExtraRunsConceededByTeam ;

const getEconomyOfBowels = (matches,deliveries,year) => {
    if(!matches || !deliveries || !year) return {}
    let economyOfBowlersObj = {}
    for(const match of matches) {
        if( year  == match.season ) {
            let matchId = match.id 
            for (const delivery of deliveries) {
                if(delivery.match_id == matchId) {
                    if (economyOfBowlersObj[delivery.bowler]) {
                        if(!(delivery.wide_runs == 1 || delivery.noball_runs == 1)) {
                            economyOfBowlersObj[delivery.bowler]['valid_balls'] += 1
                        }
                        economyOfBowlersObj[delivery.bowler]['total_runs'] += parseFloat(delivery.total_runs)
                        economyOfBowlersObj[delivery.bowler]['economy'] = ( economyOfBowlersObj[delivery.bowler]['total_runs'] * 6 / economyOfBowlersObj[delivery.bowler]['valid_balls'] ).toFixed(2)
                    } else {
                        let valid_ball = 0 ;
                        let economy = 0 ;
                        if(!(delivery.wide_runs == 1 || delivery.noball_runs == 1)) {
                            valid_ball = 1
                            economy = parseFloat(delivery.total_runs) * 6 / valid_ball
                        }
                        let economycal = [['valid_balls', valid_ball],['total_runs',parseFloat(delivery.total_runs)],['economy',    economy]]
                        economyOfBowlersObj[delivery.bowler] = Object.fromEntries(economycal)
                    }
                }
            }
        }
    }
    let economiesArray = []
    for (const each in economyOfBowlersObj) {
        economiesArray.push(parseFloat(economyOfBowlersObj[each]['economy']))
    }
    economiesArray.sort(function(a, b){return a-b})
    let top10EconomyBowlersObject = {}
    for(l=0 ; l <10 ; l++) {
        for(const each in economyOfBowlersObj) {
            if(economyOfBowlersObj[each]['economy'] == economiesArray[l]) {
                top10EconomyBowlersObject[each] = economiesArray[l]
            }
        }
    }
    return top10EconomyBowlersObject ;
}

module.exports.getEconomyOfBowels = getEconomyOfBowels ;