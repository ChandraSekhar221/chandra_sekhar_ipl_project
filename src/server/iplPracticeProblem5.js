// Find the bowler with the best economy in super overs

const csvToJsonConvertor = require('csvtojson')
const path = require('path')
const fs = require('fs')

const deliveriesCSVFilePath = path.resolve(__dirname,'../data/deliveries.csv')
const resultJsonFilePathPractice5 = path.resolve(__dirname,'../public/output/practice5.json')

const getBestBowlerOfSuperOver = (deliveries) => {
    let bestSuperOverBowlersObject = {}
    if(!deliveries) return {}
    for(const delivery of deliveries) {
        if(delivery.is_super_over == 1) {
            if(!bestSuperOverBowlersObject[delivery.bowler]) {
                bestSuperOverBowlersObject[delivery.bowler] = {'legal_balls':0, 'runs_given':0, 'economy': 0}
            }
            if(delivery.wide_runs == 0 && delivery.noball_runs == 0) {
                bestSuperOverBowlersObject[delivery.bowler]['legal_balls'] += 1 ;
            }
            bestSuperOverBowlersObject[delivery.bowler]['runs_given'] += parseFloat(delivery.total_runs)
            if(bestSuperOverBowlersObject[delivery.bowler]['legal_balls'] > 0) {
                bestSuperOverBowlersObject[delivery.bowler]['economy'] = 
                ( bestSuperOverBowlersObject[delivery.bowler]['runs_given'] * 6 /
                bestSuperOverBowlersObject[delivery.bowler]['legal_balls'] ).toFixed(2)
            }
        }
    }
    let bestEconomy = [] ;
    for(const key in bestSuperOverBowlersObject) {
        bestEconomy.push((bestSuperOverBowlersObject[key]['economy']))
    }
    bestEconomy.sort((a,b) => a-b)
    let bestSuperOverBowler = {}
    for(const key in bestSuperOverBowlersObject) {
        if (bestEconomy[0] == bestSuperOverBowlersObject[key]['economy']) {
            bestSuperOverBowler[key] = bestEconomy[0];
        }
    }
    return bestSuperOverBowler ;
}

const testCall = async () => {
    const deliveriesJSData = await csvToJsonConvertor().fromFile(deliveriesCSVFilePath).then((JsonObj) => JsonObj) ;
    const result = getBestBowlerOfSuperOver(deliveriesJSData);
    fs.writeFileSync(resultJsonFilePathPractice5,JSON.stringify(result,null,2),err => {console.log(err)})
}

testCall()
